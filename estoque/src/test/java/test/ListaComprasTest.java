package test;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ListaComprasTest {

	static WebDriver driverChrome;
	static WebDriver driverFirefox;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		System.setProperty("webdriver.chrome.driver", "C:/chromedriver/chromedriver.exe");
		System.setProperty("webdriver.gecko.driver", "C:/geckodriver/geckodriver.exe");
		//driverFirefox = new FirefoxDriver();
		//driverFirefox.get("file:///C:/Users/100931811/git/estoque-2018-2/estoque/src/main/webapp/lista-compras.html");

		 driverChrome = new ChromeDriver();
		 driverChrome.get("file:///C:Users/100931811/git/estoque-2018-2/estoque/src/main/webapp/lista-compras.html");
	}
	/*
	@Test
	public void testeFirefox() {
		
		WebElement produto = driverFirefox.findElement(By.id("produto"));
		produto.sendKeys("Manga");
		
		WebElement inputQuantidade = driverFirefox.findElement(By.id("quantidade"));
		inputQuantidade.sendKeys("10");
		
		WebElement inputValor = driverFirefox.findElement(By.id("valorUnitario"));
		inputValor.sendKeys("5");
		
		WebElement button = driverFirefox.findElement(By.id("calcularBtn"));
		button.click();
		
	}
*/
	@Test
	public void testeChrome() {
		
		int esperado = 50;
		
		WebElement produto = driverChrome.findElement(By.id("produto"));
		produto.sendKeys("Manga");
		
		WebElement inputQuantidade = driverChrome.findElement(By.id("quantidade"));
		inputQuantidade.sendKeys("10");
		
		WebElement inputValor = driverChrome.findElement(By.id("valorUnitario"));
		inputValor.sendKeys("5");
		
		WebElement button = driverChrome.findElement(By.id("calcularBtn"));
		button.click();
		
		Assert.assertEquals(esperado, button);
		
	}

	@AfterClass
	public static void setUpAfterClass() throws Exception {
		//driverFirefox.close();
		//driverFirefox.quit();

		 //driverChrome.close();
		// driverChrome.quit();
	}
}
